# i3config2hm

Converts an i3 config to home-manager i3 config. Usage:

```./i3config2hm.py input.config output.nix```

Requires python 3.7, but probably works with 3.6 as well.

Warning: this is a very hacky script that probably only works for my input config (included). Do not expect it to work without a few tweaks. Re: contributing, I'll accept simple improvements, fork for big refactors. 
