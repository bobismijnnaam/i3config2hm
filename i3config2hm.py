#!/usr/bin/env python3.7

import sys
import re
from collections import defaultdict

i3ConfPath = sys.argv[1]

with open(i3ConfPath, "r") as f:
    lines = f.readlines()

bindSymRe = re.compile(r"bindsym ([^ ]+) (.*)")
bindCodeRe = re.compile(r"bindcode ([^ ]+) (.*)")
setRe = re.compile("""set \$(.*?) (.*)""")
fontRe = re.compile("font (.*)")
execRe = re.compile("exec (--no-startup-id)(.*)")
execAlwaysRe = re.compile("exec_always (--no-startup-id)(.*)")
floatingModifierRe = re.compile("floating_modifier (.*)")
assignsRe = re.compile(r"""assign \[class="(.*)"\] (.*)""")
forWindowRe = re.compile(r"""for_window \[class="(.*)"\] (.*)""")
modeRe = re.compile(r"""mode "(.*)" {""")

class Startup:
    def __init__(self, cmd, noStartupId, always):
        self.cmd = cmd
        self.noStartupId = noStartupId
        self.always = always

    def toNix(self):
        return f"""{{
  command = "{self.cmd}";
  always = {str(self.always).lower()};
  notification = {str(not self.noStartupId).lower()};
}}"""

class WindowCommand:
    def __init__(self, cmd, classSelector):
        self.cmd = cmd
        self.classSelector = classSelector

    def toNix(self):
        return f"""{{
  command = "{self.cmd}";
  criteria = {{
    class = "{self.classSelector}"; 
  }};
}}"""

class Bar:
    def __init__(self):
        self.position = None
        self.statusCommand = None

    def toNix(self):
        return f"""{{
  position = "{self.position}";
  statusCommand = "{self.statusCommand}";
}}"""

def surround(string, char):
    return ((char if not string.startswith(char) else "")
        + string
        + (char if not string.endswith(char) else ""))

def indent(string, indent):
    return string.replace("\n", "\n" + indent)

def modeName(string):
    if "$" in string:
        return f'"{string}"'
    else:
        return string

class Mode:
    def __init__(self):
        self.sets = {}
        self.bindSyms = {}
        self.bindCodes = {}
        self.fonts = []
        self.startup = []
        self.floatingModifier = None
        self.assigns = {}
        self.windowCommands = []
        self.bar = Bar()

    def setsToNix(self):
        return "\n  ".join( f"""{k} = {surround(self.sets[k], '"')};""" for k in self.sets )

    def bindSymsToNix(self):
        for k in self.bindSyms:
            self.bindSyms[k] = self.bindSyms[k].replace(r'"', r'\"')

        return "\n    ".join( f""""{k}" = "{self.bindSyms[k]}";""" for k in self.bindSyms )

    def bindCodesToNix(self):
        return "\n    ".join( f""""{k}" = "{self.bindCodes[k]}";""" for k in self.bindCodes )

    def fontsToNix(self):
        return " ".join(surround(font, "\"") for font in self.fonts)

    def startupToNix(self):
        return "\n    ".join(indent(startup.toNix(), "    ") for startup in self.startup)

    def modeToNix(self):
        return f"""{{
    { self.bindSymsToNix() }
}}"""

    def barToNix(self):
        return f"""bars = [
    { self.bar.toNix() }
];"""

    def floatingModifierToNix(self):
        return f"""floating.modifier = "{self.floatingModifier}";"""

    def assignsToNix(self):
        return "\n".join(f""""{workspace}" = [{{ class = "{self.assigns[workspace]}"; }}];""" for workspace in self.assigns)

    def windowCommandsToNix(self):
        return "\n".join(windowCommand.toNix() for windowCommand in self.windowCommands)

    def toNix(self, modes):
        modesStr = "\n".join(f"""{modeName(mode)} = {modes[mode].modeToNix()};""" for mode in modes if mode != "default" and mode != "bar")

        return f"""let
  { self.setsToNix() }
in {{
  keybindings = {{
    { self.bindSymsToNix() }
  }};

  keycodebindings = {{
    { self.bindCodesToNix() }
  }};

  fonts = [ { self.fontsToNix() } ];

  startup = [
    { self.startupToNix() }
  ];

  modes = {{
    { modesStr }
  }};

  { self.floatingModifierToNix() if self.floatingModifier else "" }

  assigns = {{
    { self.assignsToNix() }
  }};

  window.commands = [
    { self.windowCommandsToNix() }
  ];

  { modes["bar"].barToNix() if modes["bar"] else "" }
}}
"""

def toNix(modes):
    pass

modes = defaultdict(Mode)
currentModeName = "default"
currentMode = modes[currentModeName]

inMode = False
for line in lines:
    line = line.strip()
    if len(line) == 0 or line[0] == "#":
        continue

    print("---------")
    print(line)

    bindSymMatch = bindSymRe.match(line)
    if bindSymMatch:
        print(bindSymMatch)
        currentMode.bindSyms[bindSymMatch.group(1)] = bindSymMatch.group(2)
        continue

    bindCodeMatch = bindCodeRe.match(line)
    if bindCodeMatch:
        print(bindCodeMatch)
        currentMode.bindCodes[bindCodeMatch.group(1)] = bindCodeMatch.group(2)
        continue
    
    setMatch = setRe.match(line)
    if setMatch:
        print(setMatch)
        currentMode.sets[setMatch.group(1)] = setMatch.group(2)
        continue

    fontMatch = fontRe.match(line)
    if fontMatch:
        print(fontMatch)
        currentMode.fonts += [fontMatch.group(1)]
        continue

    execMatch = execRe.match(line)
    if execMatch:
        print(execMatch)
        currentMode.startup += [Startup(execMatch.group(2), not not execMatch.group(1), False)]
        continue

    floatingModifierMatch = floatingModifierRe.match(line)
    if floatingModifierMatch:
        print(floatingModifierMatch)
        currentMode.floatingModifier = floatingModifierMatch.group(1)
        continue

    assignsMatch = assignsRe.match(line)
    if assignsMatch:
        print(assignsMatch)
        currentMode.assigns[assignsMatch.group(2)] = assignsMatch.group(1)
        continue

    forWindowMatch = forWindowRe.match(line)
    if forWindowMatch:
        print(forWindowMatch)
        currentMode.windowCommands += [WindowCommand(forWindowMatch.group(2), forWindowMatch.group(1))]
        continue

    modeMatch = modeRe.match(line)
    if modeMatch:
        print(modeMatch)
        currentModeName = modeMatch.group(1)
        currentMode = modes[currentModeName]
        continue

    if line == "}":
        print("End of mode match")
        currentModeName = "default"
        currentMode = modes[currentModeName]
        continue

    if line == "bar {":
        print("Bar match")
        currentModeName = "bar"
        currentMode = modes[currentModeName]
        continue

    if line.startswith("position "):
        print("bar position match")
        currentMode.bar.position = line.split(" ")[1]
        continue

    if line.startswith("status_command"):
        print("Statuscommand match")
        currentMode.bar.statusCommand = line.split(" ", 1)[1]
        continue

    raise Exception("Missed a line: " + line)

print("--------")

nixed = currentMode.toNix(modes)

for varName in currentMode.sets:
    nixed = nixed.replace(f"${varName}", f"${{{varName}}}")

print(nixed)

with open(sys.argv[2], "w") as f:
    f.write(nixed)

